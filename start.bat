@echo off

call update.bat

start usb.bat
start hdmi.bat
start crt.bat
start audio.bat
call logmonitor.bat

call lcd.bat
call keyboard.bat
call touchpad.bat
call config.bat
call sfc.bat
